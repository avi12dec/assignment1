import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  inputField:any;
  outputfield:any;
  title = 'formateJSON';

  constructor() {
  }

  ngOnInit() {

  }

  formatjson() {
    let result =  this.filterjson(this.inputField);
    if(typeof result == 'object') {
      let output = JSON.stringify(result);
      let pattern1 = output.replace(/,+/g,",\n");
      let pattern2 = pattern1.replace(/(\[{)+/g,"[\n {\n ");
      let pattern3 = pattern2.replace(/(},)+/g,"\n},");
      let pattern4 = pattern3.replace(/(}])+/g,"\n}\n]");
      this.outputfield = pattern4;
    } else {
      this.outputfield = result;
    }
   
  }

  filterjson(data) {
    try {
      let mainData = JSON.parse(data);
      if(mainData) {
        let levels = Object.keys(mainData);
        let temp = [];
        for(let x = 0, len=levels.length; x < len; x++) {
          let _row = mainData[levels[x]];
          for(let y of _row) {
            
            if(y.level == 0 && !y.parent_id) {
              temp.push(y);
            }
  
            if(y.level == 1 && y.parent_id == temp[0].id) {
              temp[0].children.push(y);
            }
  
            if(y.level == 2) {
              temp[0].children.forEach((v,i,a)=> {
                if(y.parent_id == v.id) {
                  v.children.push(y);
                }
              })
            }
  
          }
        }
        return temp;
      }
    } catch (error) {
      return 'Invalid JSON!!!';
    }
    
  }
}
